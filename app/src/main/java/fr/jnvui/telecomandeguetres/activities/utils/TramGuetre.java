package fr.jnvui.telecomandeguetres.activities.utils;

import android.content.Context;

import java.nio.ByteBuffer;

/**
 * Created by jips on 1/16/17.
 */

public class TramGuetre {

/*    - Start: 1o U

    - numero de guetre: 6o
    - nom de la telecomande: 6o


    - id: 1o //nom du soin

    - color1: 1o
    - frequence1: 2o
    - couleur2: 1o
    - frequence2: 2o
    - temps: 2o

    - separator : 1o

    - color1: 1o
    - frequence1: 2o
    - couleur2: 1o
    - frequence2: 2o
    - temps: 2o

    - separator : 1o

    - color1: 1o
    - frequence1: 2o
    - couleur2: 1o
    - frequence2: 2o
    - temps: 2o

    - CRC(16): 2o

    - Fin de trame: 1o Complement de U

//Tram Start
    - Start: 1o U

    - numero de guetre: 6o
    - nom de la telecomande: 6o
    - id: 1o //nom du soin
    - Start/Stop: 1o

    - Stop: 1o 1/U

    - CRC(16): 2o

//Code d'erreur
    - Start: 1o U
    - nom de la telecomande: 6o
    - error : 1o
    - Stop: 1o 1/U

    //Tram Start

- Start: 1o U

- numero de guetre: 6o
- nom de la telecomande: 6o
- id: 1o //nom du soin
- Start/Stop: 1o

- Stop: 1o 1/U

*/

    final static int OCTETS = 8;
    final static int TWO_OCTETS = 2*8;
    final static int SIX_OCTETS = 6*8;

    final static int SIZE_LOAD_TRAM = 42*8;
    final static int SIZE_START_TRAM = 18*8;
    final static int SIZE_STOP_TRAM = 18*8;

    private byte[] START_CHAR =  new byte[]{85}; // U
    private byte[] STOP_CHAR = new byte[]{122};; // Z
    private byte[] SEPARATOR = new byte[]{36};; // $
    private byte[] START_PROG = new byte[]{48};; // 1
    private byte[] STOP_PROG = new byte[]{49};; // 0
    private byte[] CRC16 = new byte[TWO_OCTETS];


    private byte[] numberGuetre = new byte[SIX_OCTETS]; //number of the guetre
    private byte[] numberCommand = new byte[SIX_OCTETS]; //unique id of the application
    private byte[] numberCare = new byte[OCTETS];

    private byte[] color1 = new byte[OCTETS];
    private byte[] frequence1 =  new byte[TWO_OCTETS];
    private byte[] color12 = new byte[OCTETS];
    private byte[] frequence12 =  new byte[TWO_OCTETS];
    private byte[] time1 = new byte[TWO_OCTETS];

    private byte[] color2 = new byte[OCTETS];
    private byte[] frequence2 =  new byte[TWO_OCTETS];
    private byte[] color22 = new byte[OCTETS];
    private byte[] frequence22 =  new byte[TWO_OCTETS];
    private byte[] time2 =  new byte[TWO_OCTETS];

    private byte[] color3 = new byte[OCTETS];
    private byte[] frequence3 =  new byte[TWO_OCTETS];
    private byte[] color32 = new byte[OCTETS];
    private byte[] frequence32 =  new byte[TWO_OCTETS];
    private byte[] time3 =  new byte[TWO_OCTETS];

    private byte[] loadProgram = new byte[SIZE_LOAD_TRAM-4*8];
    private byte[] loadProgramTram = new byte[SIZE_LOAD_TRAM];

    private byte[] tramStartProgram = new byte[SIZE_START_TRAM];
    private byte[] tramStopProgram = new byte[SIZE_STOP_TRAM];

    static TramGuetre _instance;

    public static TramGuetre getInstance() {
        if (_instance == null) {
            _instance = new TramGuetre();
        }
        return _instance;
    }


    public void setNumberGuetre(byte[] numberGuetre) {
        this.numberGuetre = numberGuetre;
    }

    public void setNumberCommand(byte[] numberCommand) {
        this.numberCommand = numberCommand;
    }

    public void setNumberCare(byte[] numberCare) {
        this.numberCare = numberCare;
    }

    public void setColor1(byte[] color1) {
        this.color1 = color1;
    }

    public void setFrequence1(byte[] frequence1) {
        this.frequence1 = frequence1;
    }

    public void setColor12(byte[] color12) {
        this.color12 = color12;
    }

    public void setFrequence12(byte[] frequence12) {
        this.frequence12 = frequence12;
    }

    public void setTime1(byte[] time1) {
        this.time1 = time1;
    }

    public void setColor2(byte[] color2) {
        this.color2 = color2;
    }

    public void setFrequence2(byte[] frequence2) {
        this.frequence2 = frequence2;
    }

    public void setColor22(byte[] color22) {
        this.color22 = color22;
    }

    public void setFrequence22(byte[] frequence22) {
        this.frequence22 = frequence22;
    }

    public void setTime2(byte[] time2) {
        this.time2 = time2;
    }

    public void setColor3(byte[] color3) {
        this.color3 = color3;
    }

    public void setFrequence3(byte[] frequence3) {
        this.frequence3 = frequence3;
    }

    public void setColor32(byte[] color32) {
        this.color32 = color32;
    }

    public void setFrequence32(byte[] frequence32) {
        this.frequence32 = frequence32;
    }

    public void setTime3(byte[] time3) {
        this.time3 = time3;
    }


    //
    public byte[] getLoadProgram() {

        ByteBuffer target = ByteBuffer.wrap(loadProgram);


        target.put(numberGuetre);
        target.put(numberCommand);
        target.put(numberCare);

        target.put(color1);
        target.put(frequence1);
        target.put(color12);
        target.put(frequence12);
        target.put(time1);

        target.put(SEPARATOR);

        target.put(color2);
        target.put(frequence2);
        target.put(color22);
        target.put(frequence22);
        target.put(time2);

        target.put(SEPARATOR);

        target.put(color3);
        target.put(frequence3);
        target.put(color32);
        target.put(frequence32);
        target.put(time3);


        ByteBuffer tram = ByteBuffer.wrap(loadProgramTram);


        tram.put(START_CHAR);

        tram.put(loadProgram);

        short crc = (short)CRC.crc16(loadProgram);
        CRC16 = shortToByteArray((short)CRC.crc16(loadProgram));
        tram.put(CRC16);

        tram.put(STOP_CHAR);


        return loadProgramTram;
    }

    public byte[] getTramStartProgram() {
        return tramStartProgram;
    }

    public byte[] getTramStopProgram() {
        return tramStopProgram;
    }

    public static byte[] shortToByteArray(short value){

        byte[] ret = new byte[2];
        ret[0] = (byte)(value & 0xff);
        ret[1] = (byte)((value >> 8) & 0xff);

        return ret;

    }

    public static String convertIntArrayToString(int[] array) {
        int length = array.length;
        char [] array1 = new char[length];

        for (int i = 0; i < length; i++) {
            // this converts a integer into a character
            array1[i] = (char) array[i];
        }

        return array1.toString();
    }

}

