package fr.jnvui.telecomandeguetres.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import fr.jnvui.telecomandeguetres.R;

public class SplashScreenActivity extends AppCompatActivity {

    private static final long DEFAULT_DELAY = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                displayLogin();
            }
        };


        Timer timer = new Timer();
        timer.schedule(timerTask, DEFAULT_DELAY);   //3000ms

        Log.d("TAG", new Date(System.currentTimeMillis()).toString());
    }

    private void displayLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
