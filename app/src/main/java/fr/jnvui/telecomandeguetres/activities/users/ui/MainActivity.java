/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.jnvui.telecomandeguetres.activities.users.ui;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.users.BasicApp;
import fr.jnvui.telecomandeguetres.activities.users.DataRepository;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;
import fr.jnvui.telecomandeguetres.activities.users.model.Product;


public class MainActivity extends AppCompatActivity {
    private int productID = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // Add product list fragment if this is first creation
        if (savedInstanceState == null) {
            ProductListFragment fragment = new ProductListFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, ProductListFragment.TAG).commit();
        }
    }

    public void addUser(View v) {

        add();

    }

    public void add() {
        final EditText input = new EditText(getApplicationContext());
        input.setTextColor(Color.BLACK);
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("");
        alertDialog.setMessage("Enter new Patient ");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                DataRepository mRepository = ((BasicApp) getApplication()).getRepository();

                                ProductEntity product = new ProductEntity();
                                product.setName(input.getText().toString());
                                product.setDescription(product.getName() + " ");
                                product.setId((int) SystemClock.elapsedRealtime());
                                mRepository.addUser(product);
                            }
                        }).start();

                        dialog.dismiss();
                    }
                });

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.show();
    }

    public void deleteUser(View v) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataRepository mRepository = ((BasicApp) getApplication()).getRepository();
                if (productID != -1) {
                    mRepository.removePatient(productID);
                    productID = -1;
                }
            }
        }).start();

    }

    /**
     * Shows the product detail fragment
     */
    public void show(Product product) {

        ProductFragment productFragment = ProductFragment.forProduct(product.getId());
        productID = product.getId();

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("product")
                .replace(R.id.fragment_container,
                        productFragment, null).commit();
    }
}
