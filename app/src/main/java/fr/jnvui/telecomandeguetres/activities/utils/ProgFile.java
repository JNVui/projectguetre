package fr.jnvui.telecomandeguetres.activities.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import fr.jnvui.telecomandeguetres.R;

/**
 * Created by jips on 3/16/17.
 */

public class ProgFile {

    private static final String TAG = "ProgFile";
    public static String PROG_FILE_NAME = "prog.json";

    public static String SKIN = "skin";
    public static String PATIENTS = "patients";
    public static String PROGRAM = "programs";

    public static String TRAM = "tram";
    public static String NAME = "name";

    public static String DESCRIPTIF = "descriptif";



    public static String getTram(Context context, String progName){

        JSONObject jsonObject = getJsonFromPath(context, PROG_FILE_NAME);

        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(PROGRAM);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonArray==null){
            return "NOK";
        }

        for(int i=0; i<jsonArray.length(); i++){
            try {
                if(progName.contains(jsonArray.getJSONObject(i).getString(NAME))){
                    JSONArray tramArray = jsonArray.getJSONObject(i).getJSONArray(TRAM);

                    byte []t = new byte[tramArray.length()];

                    for(int j = 0; j < tramArray.length(); j++) {
                        t[j] = (byte) tramArray.getInt(j);
                    }

                    return new String(t);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e){
                Log.e(TAG, " No file ", e);
                e.printStackTrace();
            }
        }

        return "error";
    }

    public static byte[] getTramByte(Context context, String progName){

        JSONObject jsonObject = getJsonFromPath(context, PROG_FILE_NAME);

        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(PROGRAM);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonArray==null){
            return null;
        }

        for(int i=0; i<jsonArray.length(); i++){
            try {
                if(progName.contains(jsonArray.getJSONObject(i).getString(NAME))){
                    JSONArray tramArray = jsonArray.getJSONObject(i).getJSONArray(TRAM);

                    byte []t = new byte[tramArray.length()];

                    for(int j = 0; j < tramArray.length(); j++) {
                        t[j] = (byte) tramArray.getInt(j);
                    }

                    return t;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e){
                Log.e(TAG, " No file ", e);
                e.printStackTrace();
            }
        }

        return null;
    }

    public static JSONObject getJsonFromPath(Context context, String name) {
        //check if file is on Documents
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Environment.DIRECTORY_DOCUMENTS + "/" + PROG_FILE_NAME);
        if (file.exists()) {
            return UtilsFile.readTextFile(context, file);

        } else {
            try {
                return new JSONObject(readFileFromRawDirectory(context, R.raw.prog));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static String readFileFromRawDirectory(Context context, int rawId) {
        InputStream is = context.getResources().openRawResource(rawId);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return writer.toString();
    }

    private static String rreadFileFromRawDirectory(Context context, int resourceId) {
        InputStream iStream = context.getResources().openRawResource(resourceId);
        ByteArrayOutputStream byteStream = null;
        try {
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteStream.toString();
    }

    public static int[] getTramIntArray(Context context, String progName) {

        JSONObject jsonObject = getJsonFromPath(context, PROG_FILE_NAME);

        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(PROGRAM);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonArray == null) {
            return null;
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                if (progName.contains(jsonArray.getJSONObject(i).getString(NAME))) {
                    JSONArray tramArray = jsonArray.getJSONObject(i).getJSONArray(TRAM);

                    int[] t = new int[tramArray.length()];

                    for (int j = 0; j < tramArray.length(); j++) {
                        t[j] = tramArray.getInt(j);
                    }

                    return t;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.e(TAG, " No file ", e);
                e.printStackTrace();
            }
        }

        return null;
    }

    public static String getDescriptif(Context context, String descriptifName){

        JSONObject jsonObject = getJsonFromPath(context, PROG_FILE_NAME);

        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(PROGRAM);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonArray==null){
            return "NOK";
        }

        for(int i=0; i<jsonArray.length(); i++){
            try {
                if(descriptifName.contains(jsonArray.getJSONObject(i).getString("name")))
                    return jsonArray.getJSONObject(i).getString(DESCRIPTIF);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return "error";
    }

    public static String getFirstValueOfJson(Context context, String field){

        JSONObject jsonObject = getJsonFromPath(context, PROG_FILE_NAME);

        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(field);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            Log.e(TAG, " No file ", e);
            e.printStackTrace();
        }

        if(jsonArray==null){
            return "NOK";
        }

        try {
            return jsonArray.getJSONObject(0).getString(NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            Log.e(TAG, " No file ", e);
            e.printStackTrace();
        }

        return "";
    }

}
