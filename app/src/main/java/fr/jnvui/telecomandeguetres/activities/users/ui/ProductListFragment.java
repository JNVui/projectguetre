/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.jnvui.telecomandeguetres.activities.users.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;
import fr.jnvui.telecomandeguetres.activities.users.model.Product;
import fr.jnvui.telecomandeguetres.activities.users.viewmodel.ProductListViewModel;
import fr.jnvui.telecomandeguetres.activities.utils.ConfigFile;
import fr.jnvui.telecomandeguetres.databinding.ListFragmentBinding;

import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_END;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_START;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.LEFTRIGHT;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.ORIENTATION;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.TOPDOWN;


public class ProductListFragment extends Fragment {

    public static final String TAG = "ProductListViewModel";
    private final ProductClickCallback mProductClickCallback = new ProductClickCallback() {

        @Override
        public void onClick(Product product) {

            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).show(product);
            }
        }
    };
    Button addUser;
    private ProductAdapter mProductAdapter;
    private ListFragmentBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);

        mProductAdapter = new ProductAdapter(mProductClickCallback);
        mBinding.productsList.setAdapter(mProductAdapter);

        addUser = (Button) mBinding.getRoot().findViewById(R.id.add_user);
        loadTheme();

        return mBinding.getRoot();
    }

    private void loadTheme() {
        JSONObject jsonObject = ConfigFile.getJsonFromPath(getContext());

        int colors[] = new int[0];
        GradientDrawable gradientDrawable1 = null, gradientDrawable2 = null, gradientDrawable3 = null, gradientDrawable4 = null, gradientDrawable5 = null, gradientDrawable6 = null, gradientDrawable7 = null;
        try {
            String t = jsonObject.getString(COLOR_START);
            int i = Color.parseColor(t);
            colors = new int[]{Color.parseColor(jsonObject.getString(COLOR_START)), Color.parseColor(jsonObject.getString(COLOR_END))};

            switch (jsonObject.getString(ORIENTATION)) {
                case TOPDOWN:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);

                    break;
                case LEFTRIGHT:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);

                    break;
                default:
                    break;
            }

            //Button
            addUser.setBackground(gradientDrawable1);

            //Background


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final ProductListViewModel viewModel =
                ViewModelProviders.of(this).get(ProductListViewModel.class);

        subscribeUi(viewModel);
    }

    private void subscribeUi(ProductListViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getProducts().observe(this, new Observer<List<ProductEntity>>() {
            @Override
            public void onChanged(@Nullable List<ProductEntity> myProducts) {
                if (myProducts != null) {
                    mBinding.setIsLoading(false);
                    mProductAdapter.setProductList(myProducts);
                } else {
                    mBinding.setIsLoading(true);
                }
                // espresso does not know how to wait for data binding's loop so we execute changes
                // sync.
                mBinding.executePendingBindings();
            }
        });
    }
}
