package fr.jnvui.telecomandeguetres.activities.utils;

/**
 * Created by jips on 11/20/17.
 */

public class DateUtil {

    public static String getStringTimeFromSec(int sec) {

        int[] time = splitToComponentTimes(sec);
        return "" + time[0] + ":" + time[1] + ":" + time[2];
    }

    public static int[] splitToComponentTimes(int biggy) {
        long longVal = (long) biggy;
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

}
