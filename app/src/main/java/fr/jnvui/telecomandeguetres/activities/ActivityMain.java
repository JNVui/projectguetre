package fr.jnvui.telecomandeguetres.activities;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.fragments.DeviceFragment;

import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.getNumberOfDevices;

/**
 * Created by jips on 4/17/17.
 */

public class ActivityMain extends AppCompatActivity {

    private static final int RQS_ENABLE_BLUETOOTH = 1;
    private static final int MY_PERMISSION_REQUEST_CONSTANT = 2;
    private static final int MY_PERMISSION_REQUEST_CONSTANT2 = 3;
    private static final int MY_PERMISSION_REQUEST_CONSTANT3 = 4;
    private static final int MY_PERMISSION_REQUEST_CONSTANT4 = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutRessourceId());

        int number_of_devices = getNumberOfDevices(getApplicationContext());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        //permission for bluetooth
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSION_REQUEST_CONSTANT);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_CONSTANT2);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_CONSTANT3);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_CONSTANT4);

        getSupportFragmentManager().beginTransaction()
                .replace(getContainerId(), new DeviceFragment())
                .commit();

        if (number_of_devices >= 2) {
            getSupportFragmentManager().beginTransaction()
                    .replace(getContainerId2(), new DeviceFragment())
                    .commit();
        }
        if (number_of_devices >= 3) {
            getSupportFragmentManager().beginTransaction()
                    .replace(getContainerId3(), new DeviceFragment())
                    .commit();
        }
        if (number_of_devices >= 4) {
            getSupportFragmentManager().beginTransaction()
                    .replace(getContainerId4(), new DeviceFragment())
                    .commit();
        }
    }


    public int getContainerId() {
        return R.id.activity_container;
    }

    public int getContainerId2() {
        return R.id.activity_container1;
    }

    public int getContainerId3() {
        return R.id.activity_container2;
    }

    public int getContainerId4() {
        return R.id.activity_container3;
    }

    public int getLayoutRessourceId() {
        return R.layout.main_layout;
    }


    @Override
    protected void onResume() {
        super.onResume();

        //check and enable bluetooth
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // Device does not support Bluetooth
            Toast.makeText(getApplicationContext(), getString(R.string.bluetooth_not_support), Toast.LENGTH_SHORT).show();
            //TODO: close application
            return;
        }

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, RQS_ENABLE_BLUETOOTH);
        } else {
            //Toast.makeText(this, "Bluetooth is already enabled", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RQS_ENABLE_BLUETOOTH && resultCode == Activity.RESULT_CANCELED) {
            //TODO close application
            Toast.makeText(getApplicationContext(), getString(R.string.bluetooth_not_activate), Toast.LENGTH_SHORT).show();
            return;
        } else if (requestCode == RQS_ENABLE_BLUETOOTH && resultCode == Activity.RESULT_OK) {
            //Toast.makeText(getApplicationContext(), getString(R.string.bluetooth_activate), Toast.LENGTH_SHORT).show();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        // just do nothing
        super.onBackPressed();
    }
}