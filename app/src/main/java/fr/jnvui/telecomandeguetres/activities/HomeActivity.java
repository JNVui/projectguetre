package fr.jnvui.telecomandeguetres.activities;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.fragments.BluetoothDeviceChoiceDialogBox;
import fr.jnvui.telecomandeguetres.activities.fragments.DressColorChoiceDialogBox;
import fr.jnvui.telecomandeguetres.activities.fragments.PatientChoiceDialogBox;
import fr.jnvui.telecomandeguetres.activities.fragments.ProgramChoiceDialogBox;
import fr.jnvui.telecomandeguetres.activities.fragments.SelectFragment;
import fr.jnvui.telecomandeguetres.activities.users.BasicApp;
import fr.jnvui.telecomandeguetres.activities.users.DataRepository;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;
import fr.jnvui.telecomandeguetres.activities.utils.ProgFile;
import fr.jnvui.telecomandeguetres.activities.utils.TramGuetre;

import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_BACKGROUND;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_END;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_START;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.LEFTRIGHT;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.ORIENTATION;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.PATIENT_type;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.PRODUCT_type;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.SKIN_type;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.TOPDOWN;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.get;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.getStartTram;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.getStopTram;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PATIENTS;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PROGRAM;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PROG_FILE_NAME;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.SKIN;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.getDescriptif;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.getFirstValueOfJson;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.getTram;
import static fr.jnvui.telecomandeguetres.activities.utils.TramGuetre.shortToByteArray;

public class HomeActivity extends AppCompatActivity implements SelectFragment.NoticeDialogListener {

    public static final UUID UUID_POTAR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final int RQS_ENABLE_BLUETOOTH = 1;
    private static final int MY_PERMISSION_REQUEST_CONSTANT = 2;
    private static final int MY_PERMISSION_REQUEST_CONSTANT2 = 3;
    private static final int MY_PERMISSION_REQUEST_CONSTANT3 = 4;
    private static final int MY_PERMISSION_REQUEST_CONSTANT4 = 5;
    private static final String TAG = "HomeActivity";

    List<ProductEntity> list;
    Button mPatientNameButton;
    Button mProgramNumberButton;
    Button mDressColorButton;
    Button mGuetreButton;
    EditText mPatientNameEditText;
    EditText mProgramNumberEditText;
    EditText mDressColorEditText;
    EditText mGuetreEditText;
    TextView mDescriptif;
    Button mStartProg;
    Button mLoadProg;
    Button mStopProg;
    BluetoothAdapter mBluetoothAdapter;
    UUID mServicesUUID;
    UUID mCharacteristicUUID;
    private ProductEntity mProductEntity;
    private BluetoothGatt mGatt, mGatt1;
    private final BluetoothGattCallback gattCallBack = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
//            super.onConnectionStateChange(gatt, status, newState);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.e("gattCallBack", "STATE CONNECTED");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.e("gattCallBack", "STATE DISCONNECTED");
                    mGatt.close();
                    mGatt = null;
                    break;
                default:
                    Log.e("getCallBack", "STATE OTHER");

            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> mServices = gatt.getServices();
            Log.e("MainActivity", "onServicesDiscovered");

            for (int i = 0; i < mServices.size(); i++) {
                Log.e("Service", i + " : " + mServices.get(i).getUuid().toString());
            }

            for (BluetoothGattCharacteristic gattCharacteristic : mServices.get(2).getCharacteristics()) {
                Log.e("Characteristics", gattCharacteristic.getUuid().toString());
            }

            mServicesUUID = mServices.get(2).getUuid();
            mCharacteristicUUID = mServices.get(2).getCharacteristics().get(0).getUuid();

            mGatt.setCharacteristicNotification(mServices.get(2).getCharacteristics().get(0), true);

            BluetoothGattDescriptor descriptor = mServices.get(2).getCharacteristics().get(0).getDescriptor(UUID_POTAR);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mGatt.writeDescriptor(descriptor);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic
                                                 characteristic, int status) {
            Log.e("onCharacteristicRead", "");


            Integer Value = characteristic.getValue()[0] & 0xff;

            Log.e("Value = ", String.valueOf(Value + 5000));

        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            int value_potar = characteristic.getValue()[0] /*& 0xff*/;
            Log.e("Value = ", String.valueOf(value_potar));
        }

    };

    //TODO remove function
    void nonEditable(){

        TextInputLayout ti;

        mPatientNameEditText.setTag(mPatientNameEditText.getKeyListener());
        mPatientNameEditText.setKeyListener(null);
        mPatientNameEditText.setText(getFirstValueOfJson(getApplicationContext(),PATIENTS));
        ti = (TextInputLayout) findViewById(R.id.select_patient_inputlayout);
        ti.setHint(get(getApplicationContext(),PATIENT_type));
        mPatientNameEditText.setImeActionLabel(get(getApplicationContext(),PATIENT_type),R.id.login);


        mProgramNumberEditText.setTag(mProgramNumberEditText.getKeyListener());
        mProgramNumberEditText.setKeyListener(null);
        mProgramNumberEditText.setText(getFirstValueOfJson(getApplicationContext(),PROGRAM));

        mDressColorEditText.setTag(mDressColorEditText.getKeyListener());
        mDressColorEditText.setKeyListener(null);
        mDressColorEditText.setText(getFirstValueOfJson(getApplicationContext(),SKIN));
        ti = (TextInputLayout) findViewById(R.id.dress_color_inputlayout);
        ti.setHint(get(getApplicationContext(),SKIN_type));
        mDressColorEditText.setImeActionLabel(get(getApplicationContext(),SKIN_type),mDressColorEditText.getImeActionId());


        mGuetreEditText.setTag(mGuetreEditText.getKeyListener());
        mGuetreEditText.setKeyListener(null);
        ti = (TextInputLayout) findViewById(R.id.choose_guetre_inputlayout);
        ti.setHint(get(getApplicationContext(),PRODUCT_type));

        mLoadProg.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //permission for bluetooth
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSION_REQUEST_CONSTANT);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_CONSTANT2);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_CONSTANT3);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_CONSTANT4);

        mPatientNameEditText = (EditText) findViewById(R.id.patient_name_edittext);
        mProgramNumberEditText = (EditText) findViewById(R.id.select_program_edittext);
        mDressColorEditText = (EditText) findViewById(R.id.dress_color_edittext);
        mGuetreEditText = (EditText) findViewById(R.id.choose_guetre_edittext);

        mDescriptif = (TextView) findViewById(R.id.descriptif);

        mLoadProg = (Button) findViewById(R.id.load_button);
        mLoadProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadProgram();
            }
        });

        mStartProg = (Button) findViewById(R.id.start_button);
        mStartProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadProgram();
                startProgram();
            }
        });

        mStopProg = (Button) findViewById(R.id.stop_button);
        mStopProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopProgram();
            }
        });

        final SelectFragment.NoticeDialogListener noticeDialogListener = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataRepository mRepository = ((BasicApp) getApplication()).getRepository();
                list = mRepository.getProductList();
            }
        }).start();

        mPatientNameButton = (Button) findViewById(R.id.select_patient_button);
        mPatientNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatientChoiceDialogBox patientChoiceDialogBox = new PatientChoiceDialogBox();
                patientChoiceDialogBox.setNoticeDialogListener(noticeDialogListener);
                if (list != null) {
                    patientChoiceDialogBox.setList(list);
                    patientChoiceDialogBox.show(getSupportFragmentManager(), "1");
                }
            }
        });

        mProgramNumberButton = (Button) findViewById(R.id.select_program_button);
        mProgramNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new ProgramChoiceDialogBox();
                newFragment.show(getSupportFragmentManager(), "2");
            }
        });

        mDressColorButton = (Button) findViewById(R.id.dress_color_button);
        mDressColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DressColorChoiceDialogBox();
                newFragment.show(getSupportFragmentManager(), "3");
            }
        });

        mGuetreButton = (Button) findViewById(R.id.choose_guetre_button);
        mGuetreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new BluetoothDeviceChoiceDialogBox();
                newFragment.show(getSupportFragmentManager(), "4");
            }
        });

        loadTheme();
        nonEditable();
    }

    @Override
    public void onClickList(DialogFragment dialog, Object obj) {
        EditText editText;
        switch (dialog.getTag()) {
            case "1":
                editText = (EditText) findViewById(R.id.patient_name_edittext);
                switchGreenState(mPatientNameButton, mPatientNameEditText);
                break;
            case "2":
                editText = (EditText) findViewById(R.id.select_program_edittext);
                switchGreenState(mProgramNumberButton, mProgramNumberEditText);
                TextView descriptifTextview = (TextView) findViewById(R.id.descriptif);
                descriptifTextview.setText(getDescriptif(getApplicationContext(),(String) obj));

                View view = this.getCurrentFocus();
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                break;
            case "3":
                editText = (EditText) findViewById(R.id.dress_color_edittext);
                switchGreenState(mDressColorButton, mDressColorEditText);
                break;
            case "4":
                editText = (EditText) findViewById(R.id.choose_guetre_edittext);
                switchGreenState(mGuetreButton, mGuetreEditText);
                break;

            default:
                editText = (EditText) findViewById(R.id.dress_color_edittext);
                break;
        }

        String s = (String) obj;
        editText.setText(s);
        loadProgram();
        dialog.dismiss();
    }

    @Override
    public void onClickList(DialogFragment dialog, Object obj, ProductEntity productEntity) {
        EditText editText = (EditText) findViewById(R.id.patient_name_edittext);
        switchGreenState(mPatientNameButton, mPatientNameEditText);
        mProductEntity = productEntity;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check and enable bluetooth
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Toast.makeText(getApplicationContext(), getString(R.string.bluetooth_not_support), Toast.LENGTH_SHORT).show();
            //TODO: close application
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, RQS_ENABLE_BLUETOOTH);
        } else {
            //Toast.makeText(this, "Bluetooth is already enabled", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RQS_ENABLE_BLUETOOTH && resultCode == Activity.RESULT_CANCELED) {
            //TODO close application
            Toast.makeText(getApplicationContext(), getString(R.string.bluetooth_not_activate), Toast.LENGTH_SHORT).show();
            return;
        } else if (requestCode == RQS_ENABLE_BLUETOOTH && resultCode == Activity.RESULT_OK) {
            //Toast.makeText(getApplicationContext(), getString(R.string.bluetooth_activate), Toast.LENGTH_SHORT).show();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadTheme(){

        JSONObject jsonObject = ProgFile.getJsonFromPath(getApplicationContext(), PROG_FILE_NAME);

        int colors[] = new int[0];
        GradientDrawable gradientDrawable1 = null,gradientDrawable2 = null,gradientDrawable3 = null,gradientDrawable4 = null,gradientDrawable5 = null,gradientDrawable6 = null,gradientDrawable7 = null;
        try {
            colors = new int[]{jsonObject.getInt(COLOR_START), jsonObject.getInt(COLOR_END)};

            switch (jsonObject.getString(ORIENTATION)){
                case TOPDOWN:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable2 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable3 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable4 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable5 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable6 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable7 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    break;
                case LEFTRIGHT:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable2 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable3 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable4 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable5 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable6 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable7 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    break;
                default:
                    break;
            }

            //Button
            mPatientNameButton.setBackground(gradientDrawable1);
            mProgramNumberButton.setBackground(gradientDrawable2);
            mDressColorButton.setBackground(gradientDrawable3);
            mGuetreButton.setBackground(gradientDrawable4);
            mStartProg.setBackground(gradientDrawable5);
            mLoadProg.setBackground(gradientDrawable6);
            mStopProg.setBackground(gradientDrawable7);

            //Background
            RelativeLayout background = (RelativeLayout) findViewById(R.id.content_home);
            background.setBackgroundColor(jsonObject.getInt(COLOR_BACKGROUND));


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            Log.e(TAG, " No file ", e);
            e.printStackTrace();
        }

    }

    private void loadProgram() {
        //first check if all field are ok
       if( !checkFieldOk()){
           return;
       }

        //second check if bluetooth is ok
        StringTokenizer tokens = new StringTokenizer(mGuetreEditText.getText().toString(), " ");
        String first = tokens.nextToken();
        String second = tokens.nextToken();
        BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(second);
        connectToDevice(device1);

        //send program to guetre
    }

    private void startProgram() {

        if( !checkFieldOk()){
            return;
        }

        //second check if bluetooth is ok

        //send program to guetre
        TramGuetre.getInstance().setNumberGuetre(new byte[]{1, 2, 3, 4, 5, 6});
        TramGuetre.getInstance().setNumberCommand(new byte[]{6, 5, 4, 3, 2, 1}); //TODO get mac adress
        TramGuetre.getInstance().setNumberCare(new byte[]{45});

        //sequence 1
        TramGuetre.getInstance().setColor1(new byte[]{10});
        TramGuetre.getInstance().setFrequence1(shortToByteArray((short) 955));
        TramGuetre.getInstance().setColor12(new byte[]{20});
        TramGuetre.getInstance().setFrequence12(shortToByteArray((short) 955));
        TramGuetre.getInstance().setTime1(shortToByteArray((short) 30));

        //sequence 2
        TramGuetre.getInstance().setColor2(new byte[]{10});
        TramGuetre.getInstance().setFrequence2(shortToByteArray((short) 955));
        TramGuetre.getInstance().setColor22(new byte[]{20});
        TramGuetre.getInstance().setFrequence22(shortToByteArray((short) 955));
        TramGuetre.getInstance().setTime2(shortToByteArray((short) 30));

        //sequence 3
        TramGuetre.getInstance().setColor3(new byte[]{10});
        TramGuetre.getInstance().setFrequence3(shortToByteArray((short) 955));
        TramGuetre.getInstance().setColor32(new byte[]{20});
        TramGuetre.getInstance().setFrequence32(shortToByteArray((short) 955));
        TramGuetre.getInstance().setTime3(shortToByteArray((short) 30));

        byte[] t = TramGuetre.getInstance().getLoadProgram();

        try {
            //write dummy
            writeCharacteristic("dummy",mGatt);

            Thread.sleep(1000);
            //TRAM
            writeCharacteristic(getTram(getApplicationContext(),mProgramNumberEditText.getText().toString()),mGatt);

            Thread.sleep(1000);
            //START
            writeCharacteristic(getStartTram(getApplicationContext()),mGatt);

            //Thread.sleep(1000);
            //writeCharacteristic(value);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void createTram() {

    }

    private void stopProgram() {
        //check if device connect
        //stop program
        //START
        writeCharacteristic(getStopTram(getApplicationContext()),mGatt);
    }

    private void switchGreenState(Button button, EditText editText) {
/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(getResources().getDrawable(R.drawable.start_button));
        }
        button.setError(null);
        editText.setError(null);*/
    }

    private void switchRedState(Button button) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(getResources().getDrawable(R.drawable.stop_button));
        }
    }

    private Boolean checkFieldOk() {
        //
        mPatientNameButton.setError(null);
        mProgramNumberButton.setError(null);
        mDressColorButton.setError(null);
        mGuetreButton.setError(null);

        mPatientNameEditText.setError(null);
        mProgramNumberEditText.setError(null);
        mDressColorEditText.setError(null);
        mGuetreEditText.setError(null);

        if (!checkPatientNameOK()) {
            mPatientNameEditText.setError(getString(R.string.invalid_name));
            mPatientNameButton.setError(getString(R.string.invalid_name));
            mPatientNameEditText.requestFocus();
            return  false;
        }
        if (!checkProgramNumberOK()) {
            mProgramNumberEditText.setError(getString(R.string.invalid_program_number));
            mProgramNumberButton.setError(getString(R.string.invalid_program_number));
            mProgramNumberEditText.requestFocus();
            return false;
        }
        if (!checkDressColorOK()) {
            mDressColorEditText.setError(getString(R.string.invalid_dress));
            mDressColorButton.setError(getString(R.string.invalid_dress));
            mDressColorEditText.requestFocus();
            return false;
        }
        if (!checkGuetreOK()) {
            mGuetreEditText.setError(getString(R.string.invalid_guetre));
            mGuetreButton.setError(getString(R.string.invalid_guetre));
            mGuetreEditText.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkPatientNameOK() {
        //TODO:check on data base if exist, if not create
        if (mPatientNameEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.invalid_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkProgramNumberOK() {
        //TODO:check on data base if exist, if not create
        if (mProgramNumberEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.invalid_program_number), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkDressColorOK() {
        //TODO:check on data base if exist, if not create
        if (mDressColorEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.invalid_dress), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkGuetreOK() {
        //TODO:check on data base if exist, if not create
        if (mGuetreEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.invalid_guetre), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    //Bluetooth PArt
    public void connectToDevice(BluetoothDevice device) {
        if (mGatt == null) {
            mGatt = device.connectGatt(this, false, gattCallBack);
        }
    }

    //Bluetooth PArt
    public void connectToDevice2(BluetoothDevice device) {
        if (mGatt1 == null) {
            mGatt1 = device.connectGatt(this, false, gattCallBack);
        }
    }

    public boolean writeCharacteristic(String value, BluetoothGatt gatt){

        //check mBluetoothGatt is available
        //E/Service: 2 : 00035b03-58e6-07dd-021a-08123a000300
        //E/Characteristics: 00035b03-58e6-07dd-021a-08123a000301

        if (gatt == null) {
            Log.e(TAG, "not connect!");
            return false;
        }
        //"00002902-58e6-07dd-021a-08123a000300""
        BluetoothGattService Service = mGatt.getService(mServicesUUID);
        if (Service == null) {
            Log.e(TAG, "service not found!");
            return false;
        }
        BluetoothGattCharacteristic charac = Service
                .getCharacteristic(mCharacteristicUUID);
        if (charac == null) {
            Log.e(TAG, "char not found!");
            return false;
        }

        charac.setValue(value);
        boolean status = gatt.writeCharacteristic(charac);
        return status;
    }

}