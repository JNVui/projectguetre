package fr.jnvui.telecomandeguetres.activities.users;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import java.util.List;

import fr.jnvui.telecomandeguetres.activities.users.db.AppDatabase;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.CommentEntity;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;

/**
 * Repository handling the work with products and comments.
 */
public class DataRepository {

    private static DataRepository sInstance;

    private final AppDatabase mDatabase;
    private MediatorLiveData<List<ProductEntity>> mObservableProducts;

    private DataRepository(final AppDatabase database) {
        mDatabase = database;
        mObservableProducts = new MediatorLiveData<>();

        mObservableProducts.addSource(mDatabase.productDao().loadAllProducts(),
                productEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableProducts.postValue(productEntities);
                    }
                });
    }

    public static DataRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
    public LiveData<List<ProductEntity>> getProducts() {
        return mObservableProducts;
    }

    public List<ProductEntity> getProductList() {
        return mDatabase.productDao().loadAllProductList();
    }

    public LiveData<ProductEntity> loadProduct(final int productId) {
        return mDatabase.productDao().loadProduct(productId);
    }

    public LiveData<List<CommentEntity>> loadComments(final int productId) {
        return mDatabase.commentDao().loadComments(productId);
    }

    public void addUser(ProductEntity productEntity) {
        mDatabase.productDao().insert(productEntity);
    }

    public void addComment(ProductEntity productEntity, String comments) {
        CommentEntity comment = new CommentEntity();
        comment.setProductId(productEntity.getId());
        comment.setText(comments);
        mDatabase.commentDao().insert(comment);
    }

    public void removePatient(int productId) {
        mDatabase.productDao().removeProduct(mDatabase.productDao().loadProductSync(productId));
    }

}
