package fr.jnvui.telecomandeguetres.activities.utils;

import android.os.SystemClock;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jips on 5/9/17.
 */

/*
   Trame[0] = 'U'    // start
   Trame[1]             // id

   Trame[2]             // color 1, step 1

   Trame[3]             // frequence en Hertz, octet low
   Trame[4]             // frequence en Hertz, octet high

   Trame[5]             // color 2, step 1

   Trame[6]             // frequence en Hertz, octet low
   Trame[7]             // frequence en Hertz, octet high

   Trame[8]             // time step 1 en seconde, octet low
   Trame[9]             // time step 1 en seconde, octet high

   Trame[10] = 0     // Separateur

   Trame[11]             // color : 1, step 2

   Trame[12]             // frequence en Hertz, octet low
   Trame[13]             // frequence en Hertz, octet high

   Trame[14]             // color : 2, step 2

   Trame[15]             // frequence en Hertz, octet low
   Trame[16]             // frequence en Hertz, octet high

   Trame[17]             // time step 2 en seconde, octet low
   Trame[18]             // time step 2 en seconde, octet high

   Trame[19] = 'U'     // stop
 */

public class Health {

    //Appareil
    String mBluetoothName = "";
    String getmBluetoothAdress = "";
    String mProgName = "";
    String mPatientName = "";
    String mComment = "";
    String mDate = "";

    State mSTATE = State.STEP1;
    //step1
    int color11, color21, freq11, freq21, time1;
    //step2
    int color12, color22, freq12, freq22, time2;
    //Prog
    private int[] mProgram = new int[20];
    private int mTimeStart = 0;
    // Ut   2 00 1 00 10   0   2 00 1 00 10    U

    public Health(int[] program, String bleName, String bleAdress, String progName, String patientName, String comment) {

        mProgram = program;
        mBluetoothName = bleName;
        getmBluetoothAdress = bleAdress;
        mPatientName = patientName;
        mComment = comment;
        mProgName = progName;
        init();
    }

    public static int crc16(final byte[] buffer, int length) {
        int crc = 0xFFFF;

        for (int j = 0; j < length; j++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return crc;

    }

    public int[] getProgram() {
        return mProgram;
    }

    public byte[] getProgramByteArray() {

        int length = getProgram().length;

        //create crc
        byte[] b = new byte[length + 2];
        for (int j = 0; j < length; j++) {
            b[j] = (byte) getProgram()[j];
        }

        int tempCrc = crc16(b, length);
        b[length] = (byte) (tempCrc % 256);
        b[length + 1] = (byte) (tempCrc / 256);

        return b;
    }


    public float getTimeStart(float mStartProgTime) {
        switch (mSTATE) {
            case STEP1:
                return (SystemClock.uptimeMillis() + 1 - mStartProgTime);
            case STEP2:
                return (SystemClock.uptimeMillis() + 1 - mStartProgTime) - time1 * 1000;
        }
        return (SystemClock.uptimeMillis() + 1 - mStartProgTime);
    }

    public float getTimeStartProgress(float mStartProgTime) {
        switch (mSTATE) {
            case STEP1:
                return mStartProgTime;
            case STEP2:
                return mStartProgTime + time1 * 1000;
        }
        return (SystemClock.uptimeMillis() + 1 - mStartProgTime);
    }

    public void setTimeStart(int mTimeStart) {
        this.mTimeStart = mTimeStart;
    }

    public JSONObject getJson() throws JSONException {

        //Create Jsonfrom data
        final String BluetoothName = "bluetooth_name";
        final String BluetoothAdress = "bluetooth_adress";
        final String ProgName = "programe_name";
        final String PatientName = "patient_name";
        final String Comment = "comment";
        final String Date = "date";

        JSONObject jsonObject = new JSONObject();

        jsonObject.put(BluetoothName, mBluetoothName);
        jsonObject.put(BluetoothAdress, getmBluetoothAdress);
        jsonObject.put(ProgName, mProgName);
        jsonObject.put(PatientName, mPatientName);
        jsonObject.put(Comment, mComment);
        jsonObject.put(Date, mDate);


        return jsonObject;
    }

    public String getFormatString() throws JSONException {

        //Create Jsonfrom data
        final String BluetoothName = "\nBluetooth Device:";
        final String BluetoothAdress = "\nBluetooth Adresse:";
        final String ProgName = "\nPrograme:";
        final String PatientName = "\nPatient:";
        final String Comment = "\nComment:";
        final String Date = "\nDate:";

        String formatString = BluetoothName + mBluetoothName +
                BluetoothAdress + getmBluetoothAdress +
                ProgName + mProgName +
                PatientName + mPatientName +
                Comment + mComment +
                Date + mDate;

        return formatString;
    }

    void init() {
        if (mProgram.length != 18) {
            return;
        }
        color11 = mProgram[2];
        freq11 = mProgram[3] * 2 + 254 * mProgram[4];
        color12 = mProgram[5];
        freq12 = mProgram[6] * 2 + 254 * mProgram[7];
        time1 = mProgram[8] * 2 + 254 * mProgram[9];

        color21 = mProgram[10];
        freq21 = mProgram[11] * 2 + 254 * mProgram[12];
        color22 = mProgram[13];
        freq22 = mProgram[14] * 2 + 254 * mProgram[15];
        time2 = mProgram[16] * 2 + 254 * mProgram[17];
    }

    public String getHealtDetails() {
        String healthDetails = "";

        healthDetails = healthDetails.concat("Step 1: color 1 =" + color11);
        healthDetails = healthDetails.concat(" freq 1 =" + freq11);
        healthDetails = healthDetails.concat(" color 2 =" + color12);
        healthDetails = healthDetails.concat(" freq 2 =" + freq12);
        healthDetails = healthDetails.concat(" time 1 =" + time1);


        healthDetails = healthDetails.concat("\nStep 2: color 1 =" + color21);
        healthDetails = healthDetails.concat(" freq 1 =" + freq21);
        healthDetails = healthDetails.concat(" color 2 =" + color22);
        healthDetails = healthDetails.concat(" freq 2 =" + freq22);
        healthDetails = healthDetails.concat(" time 1 =" + time2) + "\n";

        return healthDetails;
    }


    public int getTime() {
        return time1 + time2;
    }

    public int getStateTime(float startTime) {

        int timeSinceStart = (int) (SystemClock.uptimeMillis() + 1 - startTime) / 1000;

        if (timeSinceStart < time1) {
            mSTATE = State.STEP1;
            return time1;
        } else {
            mSTATE = State.STEP2;
            return time2;
        }
    }

    public String getSTATE() {
        switch (mSTATE) {
            case STEP2:
                return "étape 2: ";
            case STEP1:
                return "étape 1: ";
        }
        return "ERR";

    }

    public  String getProgName(){
        return mProgName;
    }

    public void setComment(String comment) {
        this.mComment = comment;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    enum State {
        STEP1,
        STEP2
    }

    //TODO DAO
}
