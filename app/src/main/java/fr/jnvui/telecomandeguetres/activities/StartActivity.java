package fr.jnvui.telecomandeguetres.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.users.ui.MainActivity;
import fr.jnvui.telecomandeguetres.activities.utils.ConfigFile;

import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_BACKGROUND;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_END;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_START;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.LEFTRIGHT;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.ORIENTATION;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.TOPDOWN;

public class StartActivity extends AppCompatActivity {

    Button healthModeButton;
    Button userMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        healthModeButton = (Button) findViewById(R.id.medecine_mode);
        userMode = (Button) findViewById(R.id.patient_mode);

        healthModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ActivityMain.class));

            }
        });

        userMode.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), MainActivity.class)));

        loadTheme();
    }

    private void loadTheme() {
        JSONObject jsonObject = ConfigFile.getJsonFromPath(getApplicationContext());


        int colors[] = new int[0];
        GradientDrawable gradientDrawable1 = null, gradientDrawable2 = null, gradientDrawable3 = null, gradientDrawable4 = null, gradientDrawable5 = null, gradientDrawable6 = null, gradientDrawable7 = null;
        try {
            String t = jsonObject.getString(COLOR_START);
            int i = Color.parseColor(t);
            colors = new int[]{Color.parseColor(jsonObject.getString(COLOR_START)), Color.parseColor(jsonObject.getString(COLOR_END))};

            switch (jsonObject.getString(ORIENTATION)) {
                case TOPDOWN:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable2 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);

                    break;
                case LEFTRIGHT:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable2 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);

                    break;
                default:
                    break;
            }

            //Button
            healthModeButton.setBackground(gradientDrawable1);
            userMode.setBackground(gradientDrawable2);

            //Background
            ConstraintLayout background = (ConstraintLayout) findViewById(R.id.start_back);
            background.setBackgroundColor(Integer.getInteger(jsonObject.getString(COLOR_BACKGROUND)));


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }
}
