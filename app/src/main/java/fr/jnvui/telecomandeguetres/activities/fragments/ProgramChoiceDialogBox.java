package fr.jnvui.telecomandeguetres.activities.fragments;

import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import org.json.JSONObject;

import java.util.ArrayList;

import fr.jnvui.telecomandeguetres.activities.utils.ProgFile;

import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PROGRAM;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PROG_FILE_NAME;

/**
 * Created by jips on 12/4/16.
 */

public class ProgramChoiceDialogBox extends SelectFragment {

    private final ArrayList<CharSequence> patientName = new ArrayList<>();
    DialogFragment dialogFragment;
    private ArrayAdapter<CharSequence> arrayAdapter;

    @Override
    public void onResume() {
        super.onResume();

        JSONObject jsonObject = ProgFile.getJsonFromPath(getContext(), PROG_FILE_NAME);

        if(jsonObject != null){
            fillDataFromConfigFile(PROGRAM,jsonObject);

        } else {
            itemsArrays.add("Programme 1");
            itemsArrays.add("Programme 2");
            itemsArrays.add("Programme 3");
            itemsArrays.add("Programme 4");
        }


        dialogFragment = this;

        arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, itemsArrays);
        mListView.setAdapter(arrayAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.onClickList(dialogFragment,itemsArrays.get(i));
            }
        });
    }

}
