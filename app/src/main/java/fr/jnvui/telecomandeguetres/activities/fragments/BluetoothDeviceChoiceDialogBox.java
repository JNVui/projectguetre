package fr.jnvui.telecomandeguetres.activities.fragments;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by jips on 12/4/16.
 */

public class BluetoothDeviceChoiceDialogBox extends SelectFragment {

    private static final long SCAN_PERIOD = 10000;
    private final ArrayList<CharSequence> guetreName = new ArrayList<>();
    private ArrayAdapter<CharSequence> arrayAdapter;
    DialogFragment dialogFragment;

    private BluetoothAdapter mBluetoothAdapter;

    private boolean mScanning;

    private Handler mHandler= new Handler(){
        @Override
        public void  handleMessage(Message msg){
            switch(msg.what){
                case 0:
                    this.removeMessages(0);
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    };
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mScanning = false;
            showProgress(false);
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        dialogFragment = this;

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.onClickList(dialogFragment,guetreName.get(i));
            }
        });

        showProgress(true);
        scanLeDevice(true);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(mRunnable, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    try {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                 if(!guetreName.contains(device.getName()+" "+device.getAddress()))
                                    guetreName.add(device.getName()+" "+device.getAddress());

                                arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, guetreName);
                                mListView.setAdapter(arrayAdapter);
                            }
                        });
                    } catch (Exception e){

                    }
                }
            };

    @Override
    public void onStop() {
        super.onStop();
        mHandler.removeCallbacks(mRunnable);
    }
}

