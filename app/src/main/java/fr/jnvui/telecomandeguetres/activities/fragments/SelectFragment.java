package fr.jnvui.telecomandeguetres.activities.fragments;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;

/**
 * Created by jips on 12/4/16.
 */

public class SelectFragment extends DialogFragment {

    private static final int MY_PERMISSION_REQUEST_CONSTANT3 = 112;
    final ArrayList<CharSequence> itemsArrays = new ArrayList<>();
    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
    ListView mListView;
    ProgressBar mProgressBar;
    Button mButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_CONSTANT3);

    }


/*    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.list_view_layout, null));

        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_layout, container, false);
        mListView = (ListView) view.findViewById(R.id.listView);
        mProgressBar = (ProgressBar) view.findViewById(R.id.load_progress);
        return view;
    }

    //add function to get callBackMethod for NoticeDialogListener
    public void setNoticeDialogListener(NoticeDialogListener listener) {
        mListener = listener;
        Log.d("", "setNoticeDialogListener: ");
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mListView.setVisibility(show ? View.GONE : View.VISIBLE);
            mListView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mListView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            mListView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void fillDataFromConfigFile(String name, JSONObject jsonObject){
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonArray==null){
            return;
        }

        for(int i=0; i<jsonArray.length(); i++){
            try {
                if(!itemsArrays.contains(jsonArray.getJSONObject(i).getString("name")))
                    itemsArrays.add(jsonArray.getJSONObject(i).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getIdFromConfigFile(String name, String section, JSONObject jsonObject){
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonArray==null){
            return null;
        }

        for(int i=0; i<jsonArray.length(); i++){
            try {
                if(jsonArray.getJSONObject(i).getString("name") == section)
                    return jsonArray.getJSONObject(i).getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onClickList(DialogFragment dialog, Object obj);

        public void onClickList(DialogFragment dialog, Object obj, ProductEntity productEntity);
    }

}
