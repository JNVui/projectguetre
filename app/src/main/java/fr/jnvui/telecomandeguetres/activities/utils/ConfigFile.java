package fr.jnvui.telecomandeguetres.activities.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import fr.jnvui.telecomandeguetres.R;

/**
 * Created by jips on 3/16/17.
 */

public class ConfigFile {

    //orientation
    public final static String TOPDOWN = "top"; //ex:Orientation.TOP_BOTTOM
    public final static String LEFTRIGHT = "left"; //ex:Orientation.TOP_BOTTOM
    public final static String TRAMSTART = "tram_start";
    public final static String TRAMSTOP = "tram_stop";
    public final static String PASSWORD = "password";

    private static final String TAG = "ConfigFile" ;
    public static String CONFIG_FILE_NAME = "config.json";
    //Product
    public static String PRODUCT_type = "product_type"; //guetre, lunette, masque ...
    public static String SKIN_type = "skin_type"; //robe if horse , skin if human ....
    public static String PATIENT_type = "patient_type"; //chevale human ...
    public static String NUMBER_OF_DEVICES = "number_of_devices"; //chevale human ...
    //Theme Color int colors[] = { 0xff255779, 0xffa6c0cd };
    public static String COLOR_BACKGROUND = "color_background"; //int
    public static String COLOR_START = "color_start"; //int
    public static String COLOR_END = "color_end"; //int
    public static String ORIENTATION = "orientation"; //ex:Orientation.TOP_BOTTOM

    public static String getStartTram(Context context){
        String s = get(context,TRAMSTART);
        return s;
    }

    public static String getStopTram(Context context){
        return get(context,TRAMSTOP);
    }


    public static JSONObject getJsonFromPath(Context context) {
        //check if file is on Documents
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Environment.DIRECTORY_DOCUMENTS + "/" + CONFIG_FILE_NAME);
        if (file.exists()) {
            return UtilsFile.readTextFile(context, file);

        } else {
            try {
                return new JSONObject(ProgFile.readFileFromRawDirectory(context, R.raw.config));
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static String get(Context context, String progName){

        JSONObject jsonObject = getJsonFromPath(context);

        try {
            return jsonObject.getString(progName);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            Log.e(TAG, " No file ", e);
            e.printStackTrace();
        }

        return "error";
    }


    public static int getNumberOfDevices(Context context) {

        JSONObject jsonObject = getJsonFromPath(context);

        try {
            return jsonObject.getInt(NUMBER_OF_DEVICES);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.e(TAG, " No file ", e);
            e.printStackTrace();
        }

        return -1;
    }

    //get number of device


}
