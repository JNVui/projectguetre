package fr.jnvui.telecomandeguetres.activities.fragments;


import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;

/**
 * Created by jips on 12/4/16.
 */

public class PatientChoiceDialogBox extends SelectFragment {

    private final ArrayList<CharSequence> patientName = new ArrayList<>();
    DialogFragment dialogFragment;
    private ArrayAdapter<CharSequence> arrayAdapter;
    private List<ProductEntity> list;

    @Override
    public void onResume() {
        super.onResume();

        if (list != null) {

            for (ProductEntity productEntity : list) {
                if (!itemsArrays.contains(productEntity.getName())) {
                    itemsArrays.add(productEntity.getName());
                }
            }

        } else {
            itemsArrays.add("Tornado");
            itemsArrays.add("Pegase");
            itemsArrays.add("jolly jumper");
        }

        dialogFragment = this;

        arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, itemsArrays);
        mListView.setAdapter(arrayAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.onClickList(dialogFragment, itemsArrays.get(i), list.get(i));
            }
        });

    }

    public List<ProductEntity> getList() {
        return list;
    }

    public void setList(List<ProductEntity> list) {
        this.list = list;
    }
}