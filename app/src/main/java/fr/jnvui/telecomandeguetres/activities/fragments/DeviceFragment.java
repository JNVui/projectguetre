package fr.jnvui.telecomandeguetres.activities.fragments;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import fr.jnvui.telecomandeguetres.R;
import fr.jnvui.telecomandeguetres.activities.fragments.SelectFragment.NoticeDialogListener;
import fr.jnvui.telecomandeguetres.activities.users.BasicApp;
import fr.jnvui.telecomandeguetres.activities.users.DataRepository;
import fr.jnvui.telecomandeguetres.activities.users.db.entity.ProductEntity;
import fr.jnvui.telecomandeguetres.activities.utils.ConfigFile;
import fr.jnvui.telecomandeguetres.activities.utils.DateUtil;
import fr.jnvui.telecomandeguetres.activities.utils.Health;

import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_BACKGROUND;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_END;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.COLOR_START;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.LEFTRIGHT;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.ORIENTATION;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.PATIENT_type;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.PRODUCT_type;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.SKIN_type;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.TOPDOWN;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.get;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.getStartTram;
import static fr.jnvui.telecomandeguetres.activities.utils.ConfigFile.getStopTram;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PATIENTS;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.PROGRAM;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.SKIN;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.getDescriptif;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.getFirstValueOfJson;
import static fr.jnvui.telecomandeguetres.activities.utils.ProgFile.getTramIntArray;

/**
 * Created by jips on 5/31/17.
 */
public class DeviceFragment extends Fragment implements NoticeDialogListener {


    public static final UUID UUID_POTAR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final String TAG = "DeviceFragment";
    private static final int TRY_CONNECT = 5;
    static Semaphore semaphore = new Semaphore(0); //TODO

    List<ProductEntity> list;
    Handler mHandler = new Handler();
    View mRootView;
    Button mPatientNameButton;
    Button mProgramNumberButton;
    Button mDressColorButton;
    Button mGuetreButton;
    EditText mPatientNameEditText;
    EditText mProgramNumberEditText;
    EditText mDressColorEditText;
    EditText mGuetreEditText;
    TextView mDescriptif;
    Button mStartProg;
    Button mLoadProg;
    Button mStopProg;
    ProgressBar mProgressBar;
    ProgressBar mProgressBar2;
    TextView mProgressBarText;
    TextView mProgressBarText2;
    String mDataBufferString = "";
    float mStartProgTime = 0;
    Health mHealth;
    BluetoothAdapter mBluetoothAdapter;
    UUID mServicesUUID;
    UUID mCharacteristicUUID;
    Boolean mIsRunning = true;
    Boolean mIsConnected = false;
    private ProductEntity mProductEntity;
    private ProgressDialog mProgressDialog;
    private BluetoothGatt mGatt, mGatt1;
    private final BluetoothGattCallback gattCallBack = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
//            super.onConnectionStateChange(gatt, status, newState);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.e("gattCallBack", "STATE CONNECTED");
                    gatt.discoverServices();

                    // Toast when a new device is connected
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity().getApplicationContext(), mGuetreEditText.getText() + " is connected", Toast.LENGTH_LONG).show();
                        }
                    });
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.e("gattCallBack", "STATE DISCONNECTED");
                    mGatt.close();
                    mGatt = null;
                    mIsConnected = false;

                    // Toast when a device is disconnected TODO bug si on quitte la page
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity().getApplicationContext(), mGuetreEditText.getText() + " is disconnected", Toast.LENGTH_LONG).show();
                        }
                    });
                    break;
                default:
                    Log.e("getCallBack", "STATE OTHER");

            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> mServices = gatt.getServices();
            Log.e("MainActivity", "onServicesDiscovered");

            for (int i = 0; i < mServices.size(); i++) {
                Log.e("Service", i + " : " + mServices.get(i).getUuid().toString());
            }

            for (BluetoothGattCharacteristic gattCharacteristic : mServices.get(2).getCharacteristics()) {
                Log.e("Characteristics", gattCharacteristic.getUuid().toString());
            }

            mServicesUUID = mServices.get(2).getUuid();
            mCharacteristicUUID = mServices.get(2).getCharacteristics().get(0).getUuid();

            mGatt.setCharacteristicNotification(mServices.get(2).getCharacteristics().get(0), true);

            BluetoothGattDescriptor descriptor = mServices.get(2).getCharacteristics().get(0).getDescriptor(UUID_POTAR);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mGatt.writeDescriptor(descriptor);
            mIsConnected = true;
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic
                                                 characteristic, int status) {
            Log.e("onCharacteristicRead", "");


            Integer Value = characteristic.getValue()[0] & 0xff;

            Log.e("Value = ", String.valueOf(Value + 5000));

        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            int value_potar = characteristic.getValue()[0] /*& 0xff*/;

            //Use a string to check the last word we receive, before TODO change mechanism
            mDataBufferString = mDataBufferString.concat("" + (char) value_potar); //we put 3digits in case
            if (value_potar == 13) {
                semaphore.release();
                Log.e("Buffer = ", "" + mDataBufferString);// + "  char" + mDataBufferString);
            } else {
                semaphore.tryAcquire();
            }
        }

    };
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            int count = 0;
            int countTimeout = 0;
            Boolean tryConnect = true;

            mDataBufferString = "";

            //launch prog
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startProgram();
                }
            }, 10);


            while (tryConnect || count <= TRY_CONNECT) {

                try {
                    //Check response if NOK, send again, else continue
                    if (!checkIfOk()) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startProgram();
                            }
                        }, 500);
                        Log.e(TAG, "check not ok");
                    } else {
                        tryConnect = false; //program is start leave while
                        mIsRunning = true;
                        progProgress(); //show bar
                        Log.e(TAG, "check ok");
                        break;
                    }

                    if (!semaphore.tryAcquire(1, TimeUnit.SECONDS)) {
                        Log.i(TAG, "countTimeout++ = " + countTimeout);
                        countTimeout++;
                    }

                    if (countTimeout >= TRY_CONNECT) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                }
                                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                                alertDialog.setTitle("Oups..");
                                alertDialog.setMessage("No response, Retry");
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.show();
                            }
                        }, 300);

                        Log.e(TAG, "no response");

                        tryConnect = false;
                        break;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    //TODO remove function
    void nonEditable() {

        if (mRootView == null) {
            return;
        }

        TextInputLayout ti;

        mPatientNameEditText.setTag(mPatientNameEditText.getKeyListener());
        mPatientNameEditText.setKeyListener(null);
        mPatientNameEditText.setText(getFirstValueOfJson(getContext(), PATIENTS));
        ti = (TextInputLayout) mRootView.findViewById(R.id.select_patient_inputlayout);
        ti.setHint(get(getContext(), PATIENT_type));
        mPatientNameEditText.setImeActionLabel(get(getContext(), PATIENT_type), R.id.login);


        mProgramNumberEditText.setTag(mProgramNumberEditText.getKeyListener());
        mProgramNumberEditText.setKeyListener(null);
        mProgramNumberEditText.setText(getFirstValueOfJson(getContext(), PROGRAM));

        mDressColorEditText.setTag(mDressColorEditText.getKeyListener());
        mDressColorEditText.setKeyListener(null);
        mDressColorEditText.setText(getFirstValueOfJson(getContext(), SKIN));
        ti = (TextInputLayout) mRootView.findViewById(R.id.dress_color_inputlayout);
        ti.setHint(get(getContext(), SKIN_type));
        mDressColorEditText.setImeActionLabel(get(getContext(), SKIN_type), mDressColorEditText.getImeActionId());


        mGuetreEditText.setTag(mGuetreEditText.getKeyListener());
        mGuetreEditText.setKeyListener(null);
        ti = (TextInputLayout) mRootView.findViewById(R.id.choose_guetre_inputlayout);
        ti.setHint(get(getContext(), PRODUCT_type));

        mLoadProg.setVisibility(View.GONE);

        mDescriptif.setText(getDescriptif(getContext(), mProgramNumberEditText.getText().toString()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.activity_home, container, false);

        mPatientNameEditText = (EditText) mRootView.findViewById(R.id.patient_name_edittext);
        mProgramNumberEditText = (EditText) mRootView.findViewById(R.id.select_program_edittext);
        mDressColorEditText = (EditText) mRootView.findViewById(R.id.dress_color_edittext);
        mGuetreEditText = (EditText) mRootView.findViewById(R.id.choose_guetre_edittext);

        mDescriptif = (TextView) mRootView.findViewById(R.id.descriptif);

        mLoadProg = (Button) mRootView.findViewById(R.id.load_button);
        mLoadProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadProgram();
            }
        });

        mStartProg = (Button) mRootView.findViewById(R.id.start_button);
        mStartProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mHealth = new Health(getTramIntArray(getContext(), mProgramNumberEditText.getText().toString()),
                        mGuetreEditText.getText().toString(),
                        mGuetreEditText.getText().toString(),
                        mProgramNumberEditText.getText().toString(),
                        mPatientNameEditText.getText().toString(),
                        "");

                // check if all field aren't empty
                if (!checkFieldOk()) {
                    return;
                }

                //if not connected try to reconnect
                if (!mIsConnected)
                    loadProgram();

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog = ProgressDialog.show(getContext(), "", "Patientez...", true, false);

                    }
                }, 10);

                new Thread(mRunnable).start();
            }
        });

        mStopProg = (Button) mRootView.findViewById(R.id.stop_button);
        mStopProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopProgram();
            }
        });

        final AppCompatActivity deviceFragment = (AppCompatActivity) getActivity();
        final SelectFragment.NoticeDialogListener noticeDialogListener = this;

        mPatientNameButton = (Button) mRootView.findViewById(R.id.select_patient_button);
        mPatientNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatientChoiceDialogBox patientChoiceDialogBox = new PatientChoiceDialogBox();
                patientChoiceDialogBox.setNoticeDialogListener(noticeDialogListener);

                if (list != null) {
                    patientChoiceDialogBox.setList(list);
                    patientChoiceDialogBox.show(deviceFragment.getSupportFragmentManager(), "1");
                }

            }
        });

        mProgramNumberButton = (Button) mRootView.findViewById(R.id.select_program_button);
        mProgramNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProgramChoiceDialogBox newFragment = new ProgramChoiceDialogBox();
                newFragment.setNoticeDialogListener(noticeDialogListener);
                newFragment.show(deviceFragment.getSupportFragmentManager(), "2");
            }
        });

        mDressColorButton = (Button) mRootView.findViewById(R.id.dress_color_button);
        mDressColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DressColorChoiceDialogBox newFragment = new DressColorChoiceDialogBox();
                newFragment.setNoticeDialogListener(noticeDialogListener);
                newFragment.show(deviceFragment.getSupportFragmentManager(), "3");
            }
        });

        mGuetreButton = (Button) mRootView.findViewById(R.id.choose_guetre_button);
        mGuetreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothDeviceChoiceDialogBox newFragment = new BluetoothDeviceChoiceDialogBox();
                newFragment.setNoticeDialogListener(noticeDialogListener);
                newFragment.show(deviceFragment.getSupportFragmentManager(), "4");
            }
        });

        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);
        mProgressBarText = (TextView) mRootView.findViewById(R.id.progress_text_1);
        mProgressBarText.setVisibility(View.GONE);

        mProgressBar2 = (ProgressBar) mRootView.findViewById(R.id.progress_bar2);
        mProgressBar2.setVisibility(View.GONE);
        mProgressBarText2 = (TextView) mRootView.findViewById(R.id.progress_text_2);
        mProgressBarText2.setVisibility(View.GONE);

        loadTheme();
        nonEditable();
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataRepository mRepository = ((BasicApp) getActivity().getApplication()).getRepository();
                list = mRepository.getProductList();
            }
        }).start();

        //check and enable bluetooth
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Toast.makeText(getContext(), getString(R.string.bluetooth_not_support), Toast.LENGTH_SHORT).show();
            //TODO: close application
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mGatt != null) {
            mGatt.close();
        }

        if (mGatt1 != null) {
            mGatt1.close();
        }
    }

    @Override
    public void onClickList(DialogFragment dialog, Object obj) {
        EditText editText;
        switch (dialog.getTag()) {
            case "1":
                editText = (EditText) mRootView.findViewById(R.id.patient_name_edittext);
                switchGreenState(mPatientNameButton, mPatientNameEditText);
                break;
            case "2":
                editText = (EditText) mRootView.findViewById(R.id.select_program_edittext);
                switchGreenState(mProgramNumberButton, mProgramNumberEditText);
                TextView descriptifTextview = (TextView) mRootView.findViewById(R.id.descriptif);
                descriptifTextview.setText(getDescriptif(getContext(), (String) obj));

                View view = getActivity().getCurrentFocus();
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                break;
            case "3":
                editText = (EditText) mRootView.findViewById(R.id.dress_color_edittext);
                switchGreenState(mDressColorButton, mDressColorEditText);
                break;
            case "4":
                editText = (EditText) mRootView.findViewById(R.id.choose_guetre_edittext);
                switchGreenState(mGuetreButton, mGuetreEditText);
                //TODO fix
                String s = (String) obj;
                editText.setText(s);
                loadProgram();
                break;

            default:
                editText = (EditText) mRootView.findViewById(R.id.dress_color_edittext);
                break;
        }

        String s = (String) obj;
        editText.setText(s);
        dialog.dismiss();
    }

    @Override
    public void onClickList(DialogFragment dialog, Object obj, ProductEntity productEntity) {
        EditText editText = (EditText) mRootView.findViewById(R.id.patient_name_edittext);
        switchGreenState(mPatientNameButton, mPatientNameEditText);
        mProductEntity = productEntity;
        String s = (String) obj;
        editText.setText(s);
        dialog.dismiss();
    }


    private void loadTheme() {
        JSONObject jsonObject = ConfigFile.getJsonFromPath(getContext());


        int colors[] = new int[0];
        GradientDrawable gradientDrawable1 = null, gradientDrawable2 = null, gradientDrawable3 = null, gradientDrawable4 = null, gradientDrawable5 = null, gradientDrawable6 = null, gradientDrawable7 = null;
        try {
            String t = jsonObject.getString(COLOR_START);
            int i = Color.parseColor(t);
            colors = new int[]{Color.parseColor(jsonObject.getString(COLOR_START)), Color.parseColor(jsonObject.getString(COLOR_END))};

            switch (jsonObject.getString(ORIENTATION)) {
                case TOPDOWN:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable2 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable3 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable4 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable5 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable6 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    gradientDrawable7 = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM, colors);
                    break;
                case LEFTRIGHT:
                    gradientDrawable1 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable2 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable3 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable4 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable5 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable6 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    gradientDrawable7 = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors);
                    break;
                default:
                    break;
            }

            //Button
            mPatientNameButton.setBackground(gradientDrawable1);
            mProgramNumberButton.setBackground(gradientDrawable2);
            mDressColorButton.setBackground(gradientDrawable3);
            mGuetreButton.setBackground(gradientDrawable4);
            mStartProg.setBackground(gradientDrawable5);
            mLoadProg.setBackground(gradientDrawable6);
            mStopProg.setBackground(gradientDrawable7);

            //Background
            RelativeLayout background = (RelativeLayout) mRootView.findViewById(R.id.content_home);
            background.setBackgroundColor(Integer.getInteger(jsonObject.getString(COLOR_BACKGROUND)));


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.e(TAG, " No file ", e);
            e.printStackTrace();
        }

    }

    private void loadProgram() {

        if (!checkFieldOk()) {
            return;
        }

        Log.i(TAG, "Begin loadProgram()");

        //second check if bluetooth is ok
        StringTokenizer tokens = new StringTokenizer(mGuetreEditText.getText().toString(), " ");
        String first = tokens.nextToken();
        String second = tokens.nextToken();

        BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(second);
        connectToDevice(device1, true);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //send program to guetre
        //write dummy
        writeCharacteristic("dummy", mGatt);
        Log.i(TAG, "End loadProgram()");

    }

    private void startProgram() {

        Log.i(TAG, "Begin startProgram()");

        if (!checkFieldOk()) {
            return;
        }

        //second check if bluetooth is ok
        mDataBufferString = "";
        try {
            //TRAM
            //writeCharacteristic(getTram(getContext(), mProgramNumberEditText.getText().toString()), mGatt);
            writeCharacteristicTestU(mHealth.getProgramByteArray(), mGatt);
            Thread.sleep(100);

            //START
            writeCharacteristic(getStartTram(getContext()), mGatt);


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "End startProgram()");

    }

    private void progProgress() {
        //if(checkIfOk()){
        mProgressDialog.dismiss();
        mStartProgTime = SystemClock.uptimeMillis();
        mHealth.setTimeStart((int) (mStartProgTime));
        mHealth.setDate(new Date(System.currentTimeMillis()).toString());
        mHandler.post(new Runnable() {
            public void run() {
                if (!mIsRunning) {
                    return;
                }

                int percentState, percent = 0;
                if (mHealth != null) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBarText.setVisibility(View.VISIBLE);

                    mProgressBar2.setVisibility(View.VISIBLE);
                    mProgressBarText2.setVisibility(View.VISIBLE);


                    percent = updateProgressBarPercent(mStartProgTime, (float) mHealth.getTime());

                    float test = mHealth.getTimeStartProgress(mStartProgTime);

                    percentState = updateProgressBarPercent(test, (float) mHealth.getStateTime(mStartProgTime));

                    int timeStart = (int) (SystemClock.uptimeMillis() + 1 - mStartProgTime);

                    int restTime = (int) (mHealth.getTime() - timeStart / 1000);
                    int restTimeState = (int) (mHealth.getStateTime(mStartProgTime) - mHealth.getTimeStart(mStartProgTime) / 1000);
                    //int restTimeState = (int) (mHealth.getStateTime((int) mStartProgTime) - mHealth.getTimeStart()/1000);

                    mProgressBarText.setText(getText(R.string.rest_time) + " " + DateUtil.getStringTimeFromSec(restTime));
                    mProgressBarText2.setText(getText(R.string.rest_time) + " " + mHealth.getSTATE() + " " + DateUtil.getStringTimeFromSec(restTimeState));

                    if (percent < 100) {
                        mProgressBar.setProgress(percent);
                        mProgressBar2.setProgress(percentState);
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        mHandler.postDelayed(this, 1000);
                    } else {
                        final EditText input = new EditText(getContext());

                        stopProgram();

                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle("" + mHealth.getProgName());
                        alertDialog.setMessage("Prog Finish ");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        mHealth.setComment(input.getText().toString());
                                        try {
                                            // String data = mHealth.getJson().toString();
                                            String data = mHealth.getFormatString();
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    DataRepository mRepository = ((BasicApp) getActivity().getApplication()).getRepository();
                                                    mRepository.addComment(mProductEntity, data);

                                                }
                                            }).start();

                                            //create new file in doc a save it
                                            //UtilsFile.saveFile(getContext(),file.getAbsolutePath()+"/"+System.currentTimeMillis(),data);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                });
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT);
                        input.setLayoutParams(lp);
                        alertDialog.setView(input);

                        mProgressBar.setProgress(percent);
                        alertDialog.show();
                    }
                }
            }
        });

    }

    public void ToastPost(String data) {
        Toast.makeText(getActivity().getApplicationContext(), data, Toast.LENGTH_SHORT).show();
    }


    private boolean checkIfOk() {
        Log.i(TAG, "checkIfOK()");
        Log.i(TAG, "Buffer =\n" + mDataBufferString);
        if (mDataBufferString.contains("N") || mDataBufferString.isEmpty()) {
            mDataBufferString = "";
            return false;
        } else {
            mDataBufferString = "";
            return true;
        }
    }


    private void stopProgress() {
        hideProgressBar();
    }

    private void stopProgram() {
        //check if device connect
        stopProgress();
        mIsRunning = false;
        writeCharacteristic(getStopTram(getContext()), mGatt);

    }

    private void switchGreenState(Button button, EditText editText) {
/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(getResources().getDrawable(R.drawable.start_button));
        }
        button.setError(null);
        editText.setError(null);*/
    }

    private void switchRedState(Button button) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(getResources().getDrawable(R.drawable.stop_button));
        }
    }

    private void hideProgressBar() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgressBarText.setVisibility(View.GONE);
                mProgressBarText2.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                mProgressBar2.setVisibility(View.GONE);
            }
        }, 500);

    }

    private Boolean checkFieldOk() {
        //
        mPatientNameButton.setError(null);
        mProgramNumberButton.setError(null);
        mDressColorButton.setError(null);
        mGuetreButton.setError(null);

        mPatientNameEditText.setError(null);
        mProgramNumberEditText.setError(null);
        mDressColorEditText.setError(null);
        mGuetreEditText.setError(null);

        if (!checkPatientNameOK()) {
            mPatientNameEditText.setError(getString(R.string.invalid_name));
            mPatientNameButton.setError(getString(R.string.invalid_name));
            mPatientNameEditText.requestFocus();
            return false;
        }
        if (!checkProgramNumberOK()) {
            mProgramNumberEditText.setError(getString(R.string.invalid_program_number));
            mProgramNumberButton.setError(getString(R.string.invalid_program_number));
            mProgramNumberEditText.requestFocus();
            return false;
        }
        if (!checkDressColorOK()) {
            mDressColorEditText.setError(getString(R.string.invalid_dress));
            mDressColorButton.setError(getString(R.string.invalid_dress));
            mDressColorEditText.requestFocus();
            return false;
        }
        if (!checkGuetreOK()) {
            //mGuetreEditText.setError(getString(R.string.invalid_guetre));
            //mGuetreButton.setError(getString(R.string.invalid_guetre));
            mGuetreEditText.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkPatientNameOK() {
        //TODO:check on data base if exist, if not create
        if (mPatientNameEditText.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.invalid_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkProgramNumberOK() {
        //TODO:check on data base if exist, if not create
        if (mProgramNumberEditText.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.invalid_program_number), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkDressColorOK() {
        //TODO:check on data base if exist, if not create
        if (mDressColorEditText.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.invalid_dress), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkGuetreOK() {
        //TODO:check on data base if exist, if not create
        if (mGuetreEditText.getText().toString().isEmpty()) {
            return false;
        }
        return true;
    }

    //Bluetooth PArt
    public void connectToDevice(BluetoothDevice device, boolean newconnection) {
        if (mGatt == null) {
            mGatt = device.connectGatt(getContext(), false, gattCallBack);
        } else if (newconnection) {
            mGatt = device.connectGatt(getContext(), false, gattCallBack);
        }
    }

    //Bluetooth PArt
    public void connectToDevice2(BluetoothDevice device) {
        if (mGatt1 == null) {
            mGatt1 = device.connectGatt(getContext(), false, gattCallBack);
        }
    }

    public boolean writeCharacteristic(String value, BluetoothGatt gatt) {

        //check mBluetoothGatt is available
        //E/Service: 2 : 00035b03-58e6-07dd-021a-08123a000300
        //E/Characteristics: 00035b03-58e6-07dd-021a-08123a000301

        if (gatt == null) {
            Log.e(TAG, "not connect!");
            return false;
        }
        //"00002902-58e6-07dd-021a-08123a000300""
        BluetoothGattService Service = mGatt.getService(mServicesUUID);
        if (Service == null) {
            Log.e(TAG, "service not found!");
            return false;
        }
        BluetoothGattCharacteristic charac = Service
                .getCharacteristic(mCharacteristicUUID);
        if (charac == null) {
            Log.e(TAG, "char not found!");
            return false;
        }

        charac.setValue(value);
        boolean status = gatt.writeCharacteristic(charac);
        return status;
    }

    public boolean writeCharacteristicTestU(byte[] tram, BluetoothGatt gatt) {

        //check mBluetoothGatt is available
        //E/Service: 2 : 00035b03-58e6-07dd-021a-08123a000300
        //E/Characteristics: 00035b03-58e6-07dd-021a-08123a000301
        if (tram == null) {
            Log.e(TAG, "not connect!");
            return false;
        }
        if (gatt == null) {
            Log.e(TAG, "not connect!");
            return false;
        }
        //"00002902-58e6-07dd-021a-08123a000300""
        BluetoothGattService Service = mGatt.getService(mServicesUUID);
        if (Service == null) {
            Log.e(TAG, "service not found!");
            return false;
        }
        BluetoothGattCharacteristic charac = Service
                .getCharacteristic(mCharacteristicUUID);
        if (charac == null) {
            Log.e(TAG, "char not found!");
            return false;
        }

        charac.setValue(tram);
        boolean status = gatt.writeCharacteristic(charac);
        return status;
    }

    private int updateProgressBarPercent(float time_start, float timeStop) {

        int i = (int) (SystemClock.uptimeMillis() + 1 - time_start);
        return (int) (((float) (SystemClock.uptimeMillis() + 1 - time_start) / (float) (timeStop * 1000)) * 100); //avoid division by 0

    }

}
