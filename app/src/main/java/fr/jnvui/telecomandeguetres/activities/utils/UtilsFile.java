package fr.jnvui.telecomandeguetres.activities.utils;

import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by jips on 12/14/16.
 */

public class UtilsFile {

    public static void saveFile(Context context, String filename, String string){

        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject readTextFile(Context ctx, File file) {
//Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
            e.printStackTrace();
        }
        JSONObject jObject = null;
        try {
            // Parse the data into jsonobject to get original data in form of json.
            jObject = new JSONObject(
                   text.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jObject;
    }


    public static void copyFilesToDirectory(File in, File out) {
        File[] files = in.listFiles();

        if (!out.exists()) {
            if (out.mkdirs()) {
            }
        }

        if (files != null) {


            for (int i = 0; i < files.length; i++) {
                if (files[i].getName().contains("json")) {
                    File distFile = new File(out, files[i].getName());
                    try {
                        copyFile(files[i], distFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    public static boolean copyFile(File src, File dst) throws IOException {
        if (src.getAbsolutePath().toString().equals(dst.getAbsolutePath().toString())) {
            return true;
        } else {
            InputStream is = new FileInputStream(src);
            OutputStream os = new FileOutputStream(dst);
            byte[] buff = new byte[1024];
            int len;
            while ((len = is.read(buff)) > 0) {
                os.write(buff, 0, len);
            }
            is.close();
            os.close();
        }
        return true;
    }


}
